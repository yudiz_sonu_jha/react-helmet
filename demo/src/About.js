import React from 'react'
import { Card, Layout } from 'antd';
import './App.css'
import { Link } from 'react-router-dom';

const { Meta } = Card;


function About() {
  return (
    <div className='about'>
      <Layout>
       <h1>Bollywood Collection</h1>
        <div style={{display:"flex"}}>
             <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/610/1160610-v-7ea34ec67657" />}
            >
              <Meta title="83"  />
        </Card>,
       <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/2733/1122733-v-5df3bdae2d7a" />}
            >
              <Meta title="Rudra"  />
        </Card>,

       <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/896/580896-v" />}
            >
              <Meta title="Chhichhore"  />
        </Card>,

         <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/1529/571529-v" />}
            >
              <Meta title="MIssion Mnagal"  />
        </Card>
    

            
        </div>
 
      
      </Layout>  
               

        <Link  to="/contact">go to conact page</Link>
    </div>

    
  )
}

export default About