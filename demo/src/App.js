import './App.css';
import { BrowserRouter, Route} from "react-router-dom";
import { Switch } from 'react-router-dom';
import About from './About';
import Contact from './Contact';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
// { }
function App() {
  return (
    <div className="App">
     <Helmet>
       <title>Home page</title>
       <meta name="description" content="React-Helmet-Description" />
     </Helmet>
      
      <BrowserRouter>
        <Switch>
          <Route  exact path="/" component={About}/>
          <Route path="/contact" component={Contact}/>
        </Switch>    
      

      </BrowserRouter>
    </div>
  );
}

export default App;
