import React from 'react'
import { Card, Layout } from 'antd';
import './App.css'
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';

const { Meta } = Card;


function Contact() {
  return (
    <div className='about'>
      <Helmet>
       <title>Contact</title>
       <meta name="description" content="React-Helmet-Description" />
     </Helmet>
      <Layout>
       <h1>Hollywood Collection</h1>
        <div style={{display:"flex"}}>
             <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/old_images/vertical/MOVIE/5391/1770015391/1770015391-v" />}
            >
              <Meta title="X-MEN"  />
        </Card>,
       <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/old_images/vertical/MOVIE/445/1770000445/1770000445-v" />}
            >
              <Meta title="THE-WOUVERINE "  />
        </Card>,

       <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/88/1100088-v-b96714f721d0" />}
            >
              <Meta title="ETERNALS"  />
        </Card>,

         <Card
              hoverable
              style={{ width: 240,margin:"50px"}}
              cover={<img alt="example" src="https://img1.hotstarext.com/image/upload/f_auto,t_web_vl_3x/sources/r1/cms/prod/2949/1052949-v-f2364f3b06ab" />}
            >
              <Meta title="BLACK WIDOW"  />
        </Card>
    

            
        </div>
 
      
      </Layout>  
               

        <Link  to="/">go to conact page</Link>
    </div>

    
  )
}

export default Contact